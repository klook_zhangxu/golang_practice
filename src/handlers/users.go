// User is the handler of http request

package handlers

import (
	"models"
)

type User struct {
	Id       int64  `json:"user_id"`
	Name     string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
	Status   string `json:"status,omitempty"`
}

func UserQueryHandler(userId int64) (user User, errMsg string) {
	name, err := models.QueryUser(userId)
	switch {
	case err != nil:
		errMsg = "数据库错误"
	case name == "":
		errMsg = "用户不存在"
	default:
		user.Id = userId
		user.Name = name
	}
	return
}

func UserAddHandler(username string, password string) (user User, errMsg string) {
	userId, err := models.AddUser(username, password)
	switch {
	case err != nil:
		errMsg = "数据库错误"
	case userId == 0:
		errMsg = "用户名已存在"
	default:
		user.Id = userId
		user.Name = username
	}
	return
}

func UserModifyHandler(userId int64, password string) (errMsg string) {
	rowCnt, err := models.ModifyUser(userId, password)
	if err != nil {
		errMsg = "数据库错误"
		return
	}
	if rowCnt < 0 {
		errMsg = "密码错误"
		return
	}
	return
}

func UserDeleteHandler(userId int64, password string) (errMsg string) {
	rowCnt, err := models.DeleteUser(userId, password)
	if err != nil {
		errMsg = "数据库错误"
		return
	}
	if rowCnt <= 0 {
		errMsg = "密码错误"
		return
	}
	return
}
