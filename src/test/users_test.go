// users_test is users unit test.

package test

import (
	"math/rand"
	"models"
	"strconv"
	"testing"
	"time"
)

var tmpId int64
var tmpPass string

func TestQueryUser(t *testing.T) {
	_ = models.InitDatabase()
	defer models.CloseDatabase()
	var id int64 = 1
	name, err := models.QueryUser(id)
	if err != nil {
		t.Error("query error")
	}
	if name == "" {
		t.Error("no1 user not exist.")
	}

	for _, id := range []int64{-10, 0, 9999} {
		n, err := models.QueryUser(id)
		if err != nil {
			t.Error("query error")
		}
		if n != "" {
			t.Error("user should not exist")
		}
	}
}

func TestAddUser(t *testing.T) {
	_ = models.InitDatabase()
	defer models.CloseDatabase()

	rand.Seed(time.Now().UnixNano())
	id := strconv.FormatInt(rand.Int63n(9999), 10)
	var (
		username = "test" + id
		password = "123456"
	)
	userId, err := models.AddUser(username, password)
	if userId == 0 && err != nil {
		t.Error("add user failed:", userId)
	} else {
		tmpId = userId
	}
	anoherId, err := models.AddUser(username, password)
	if anoherId != 0 {
		t.Error("duplicate add user:", userId)
	}
}

func TestModifyUser(t *testing.T) {
	_ = models.InitDatabase()
	defer models.CloseDatabase()

	userId := tmpId
	rand.Seed(time.Now().UnixNano())
	password := strconv.FormatInt(rand.Int63n(999999), 10)

	rows, err := models.ModifyUser(userId, password)
	if rows == 0 || err != nil {
		t.Error("modify password failed:", userId)
	} else {
		tmpPass = password
	}

	rows, _ = models.ModifyUser(userId, password)
	if rows > 0 {
		t.Error("duplicate modify password:", userId, password)
	}
}

func TestDeleteUser(t *testing.T) {
	_ = models.InitDatabase()
	defer models.CloseDatabase()

	userId := tmpId
	password := tmpPass

	rows, err := models.DeleteUser(userId, password)
	if rows == 0 && err != nil {
		t.Error("delete user failed:", userId, password)
	}
	rows, _ = models.DeleteUser(userId, password)
	if rows != 0 {
		t.Error("duplicate delete user:", userId, password)
	}
}
