// Users is the user database logic.

package models

import (
	"database/sql"
	"github.com/go-sql-driver/mysql"
	"log"
)

const UserStatusDisabled int = 0
const UserStatusEnabled int = 1

func QueryUser(userId int64) (username string, err error) {
	err = DbConn.QueryRow("select name from users where id = ? and status = ?", userId, UserStatusEnabled).Scan(&username)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		} else {
			log.Println(err)
		}
	}
	return
}

func AddUser(username string, password string) (userId int64, err error) {
	res, err := DbConn.Exec("insert into users (name, password, status) values (?, ?, ?)",
		username, password, UserStatusEnabled)
	if err != nil {
		if err.(*mysql.MySQLError).Number == 1062 {
			err = nil
			return
		}
		log.Println(err)
		return
	}
	lastId, err := res.LastInsertId()
	if err != nil {
		log.Println(err)
	}
	userId = lastId
	return
}

func ModifyUser(userId int64, password string) (rowCnt int64, err error) {
	err = DbConn.QueryRow("select password from users where id = ? and password = ? for update", userId, password).Scan()
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			rowCnt = -1
			return
		}
	}
	res, err := DbConn.Exec("update users set password=? where id = ?", password, userId)
	if err != nil {
		log.Println(err)
		return
	}
	rowCnt, err = res.RowsAffected()
	if err != nil {
		log.Println(err)
	}
	return
}

func DeleteUser(userId int64, password string) (rowCnt int64, err error) {
	err = DbConn.QueryRow("select password from users where id = ? and password = ? for update", userId, password).Scan()
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			return
		}
	}

	res, err := DbConn.Exec("update users set status = ? where id=? and password=?", UserStatusDisabled, userId, password)
	if err != nil {
		log.Println(err)
		return
	}

	rowCnt, err = res.RowsAffected()
	if err != nil {
		log.Println(err)
	}
	return
}
