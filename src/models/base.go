// Base is the database base logic.

package models

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

var DbConn *sql.DB

func InitDatabase() error {
	var err error
	DbConn, err = sql.Open("mysql", "root:klook_root@tcp(localhost:3306)/mydb?charset=utf8")
	if err != nil {
		log.Fatal(err)
	}
	return err
}

func CloseDatabase() {
	err := DbConn.Close()
	if err != nil {
		log.Fatal(err)
	}
}
