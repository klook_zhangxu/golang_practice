// Route handles the url dispatch.

package routes

import (
	"encoding/json"
	"handlers"
	"log"
	"net/http"
	"strconv"
)

type ApiMsg struct {
	Msg string `json:"msg,omitempty"`
}

func UsersRoutes(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		// curl -X GET "127.0.0.1:8000/users/?id=1"
		userQuery(w, r)
	case "POST":
		// curl -X POST "127.0.0.1:8000/users/" -H "Content-Type=application/x-www-form-urlencoded" -d "username=test&password=123456"
		userAdd(w, r)
	case "PUT":
		// curl -X PUT "127.0.0.1:8000/users/" -H "Content-Type=application/x-www-form-urlencoded" -d "id=1&password=654321"
		userModify(w, r)
	case "DELETE":
		// curl -X DELETE "127.0.0.1:8000/users/" -H "Content-Type=application/x-www-form-urlencoded" -d "id=10&password=654321"
		userDelete(w, r)
	}
}

func handleApiMsg(w http.ResponseWriter, msg string) {
	err := json.NewEncoder(w).Encode(ApiMsg{msg})
	if err != nil {
		log.Fatal(err)
	}
}

func userQuery(w http.ResponseWriter, r *http.Request) {
	var errMsg string
	if len(r.URL.Query()["id"]) == 0 {
		errMsg = "参数不足"
		handleApiMsg(w, errMsg)
		return
	}
	userId, err := strconv.ParseInt(r.URL.Query()["id"][0], 10, 64)
	if err != nil {
		errMsg = "用户id错误"
		handleApiMsg(w, errMsg)
		return
	}

	user, errMsg := handlers.UserQueryHandler(userId)
	if errMsg != "" {
		handleApiMsg(w, errMsg)
		return
	}
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		log.Fatal(err)
	}
}

func userAdd(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Fatal(err)
	}
	var errMsg string
	if r.FormValue("username") == "" || r.FormValue("password") == "" {
		errMsg = "参数不足"
		handleApiMsg(w, errMsg)
		return
	}

	user, errMsg := handlers.UserAddHandler(r.FormValue("username"), r.FormValue("password"))
	if errMsg != "" {
		handleApiMsg(w, errMsg)
		return
	}
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		log.Fatal(err)
	}
}

func userModify(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Fatal(err)
	}

	var errMsg string

	userId, err := strconv.ParseInt(r.FormValue("id"), 10, 64)
	if err != nil {
		errMsg = "用户id错误"
		handleApiMsg(w, errMsg)
		return
	}

	if r.FormValue("password") == "" {
		errMsg = "参数不足"
		handleApiMsg(w, errMsg)
		return
	}

	errMsg = handlers.UserModifyHandler(userId, r.FormValue("password"))
	if errMsg != "" {
		handleApiMsg(w, errMsg)
		return
	}
	w.WriteHeader(204)
}

func userDelete(w http.ResponseWriter, r *http.Request) {
	r.Method = "POST"
	err := r.ParseForm()
	if err != nil {
		log.Fatal(err)
	}

	var errMsg string

	userId, err := strconv.ParseInt(r.FormValue("id"), 10, 64)
	if err != nil {
		errMsg = "用户id错误"
		handleApiMsg(w, errMsg)
		return
	}

	if r.FormValue("password") == "" {
		errMsg = "参数不足"
		handleApiMsg(w, errMsg)
		return
	}

	errMsg = handlers.UserDeleteHandler(userId, r.FormValue("password"))
	if errMsg != "" {
		handleApiMsg(w, errMsg)
		return
	}
	handleApiMsg(w, "删除成功")
}
