// Server is entrance of this simple web server.

package main

import (
	"flag"
	"fmt"
	"log"
	"models"
	"net/http"
	"os"
	"routes"
)

func main() {

	var host = flag.String("host", "0.0.0.0", "监听地址[0.0.0.0/localhost]")
	var port = flag.Int("port", 8000, "程序监听端口")
	var mode = flag.String("mode", "simple", "启动模式[simple/mux]")

	flag.Usage = func() {
		_, _ = fmt.Fprintf(os.Stderr, "[usage] [-host] [-port] [-mode] [-h]\n")
		flag.PrintDefaults()
	}
	flag.Parse()

	addr := fmt.Sprintf("%s:%d", *host, *port)

	err := models.InitDatabase()
	if err != nil {
		fmt.Println("数据库初始化失败")
		os.Exit(0)
	}
	defer models.CloseDatabase()

	log.Printf("Server starts at %s with **%s** mode.\n", addr, *mode)

	http.HandleFunc("/users/", routes.UsersRoutes)
	// http.HandleFunc("/", http.NotFound)
	log.Fatal(http.ListenAndServe(addr, nil))
}
